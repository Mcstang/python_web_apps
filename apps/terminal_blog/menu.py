from database import Database
from model.blog import Blog

class Menu(object):
    def __init__(self):
        self.user = input("Enter your author name: ")
        self.user_blog = None
        if self._user_has_account():
            print("welcome back {}".format(self.user))
        else:
            self._prompt_used_for_account()

    def _user_has_account(self):
        blog = Database.find_one('blogs', {'author':self.user})
        if blog is not None:
            self.user_blog = Blog.from_mongo
            return True
        else:
            return False

    def _prompt_used_for_account(self):
        title = input("Enter blog title: ")
        description = input("Enter blog description: ")
        blog = Blog(author = self.user,
                    title = title, 
                    description =description
        )
        blog.save_to_mongo()
        self.user_blog = blog
    
    def run_menu(self):
        read_or_write = input("Please enter do you want to write (W) or read (R): ")
        if read_or_write == 'R':
            self._list_blogs()
            self._view_blogs()
        elif read_or_write == 'W':
            self.user_blog.new_post()
        else:
            print("Thank you for blogging")
    

    
    def _list_blogs(self):
        blogs = Database.find(collection = 'blogs', 
                            query = {})
        for blog in blogs:
            print("ID: {}, Title: {}, Author: {}".format(blog['id'], blog['title'], blog['author']))

    def _view_blogs(self):
        blog_to_see = input("Enter the Id of blog you like to read: ")
        blog = Blog.get_from_mongo(blog_to_see)
        posts = blog.get_posts()
        for post in posts:
            print("Date: {}, title: {}\n\n{}".format(post['created-date'], post['title'], post['content']))   