from database import Database
from model.post import Post
from menu import Menu

Database.initialize()

menu = Menu()
menu.run_menu()