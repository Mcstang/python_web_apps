import pymongo


############# This test_app is for test the MongoDB is working #################

uri = "mongodb://127.0.0.1:27017"
client = pymongo.MongoClient(uri)
database = client['school']
collection = database['students']

# students = collection.find({})
students = [student for student in collection.find({})]

# for student in students:
#     print(student)
print(students)